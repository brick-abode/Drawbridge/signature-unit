import os
import hashlib
import shutil
import itertools
from enum import Enum

SIGUNIT_DIR = os.path.join(os.environ["HOME"],
                           ".local/share/drawbridge/sigunit")

OUTPUT_DIR = os.path.join(os.environ["HOME"],
                          ".local/share/drawbridge/output")


class ContentsComparison(Enum):
    EMPTY = 0
    DIFFERENT = 1
    EQUAL = 2

    def are_empty(self):
        return self == self.EMPTY

    def are_different(self):
        return self == self.DIFFERENT

    def are_equal(self):
        return self == self.EQUAL


def sha256sum(filename):
    h = hashlib.sha256()
    b = bytearray(128 * 1024)
    mv = memoryview(b)
    with open(filename, 'rb', buffering=0) as f:
        for n in iter(lambda: f.readinto(mv), 0):
            h.update(mv[:n])
    return h.hexdigest()


def init_buses():
    for d in [SIGUNIT_DIR, OUTPUT_DIR]:
        if not os.path.isdir(d):
            os.makedirs(d)


def force_remove(filepath):
    if os.path.isfile(filepath) or os.path.isdir(filepath):
        os.remove(filepath)


def get_directories():
    return [os.path.join(SIGUNIT_DIR, x, "output")
            for x in os.listdir(SIGUNIT_DIR)
            if os.path.isdir(os.path.join(SIGUNIT_DIR, x))]


def get_contents(directories):
    for d in directories:
        if not os.path.isdir(d):
            os.makedirs(d)
    return [sorted([os.path.join(sig_unit, f) for f in os.listdir(sig_unit)])
            for sig_unit in directories]


def contents_synced(contents):
    if not contents:
        return False
    return all([len(x) == len(contents[0]) for x in contents])


def lock_drawbridge():
    print("Drawbridge locked")


def compare_signatures(files):
    return all([sha256sum(files[0]) == sha256sum(x) for x in files])


def send_to_output(files):
    if files:
        for f in files:
            basename = os.path.basename(f)
            shutil.move(f, os.path.join(OUTPUT_DIR, basename))


def clean(files):
    for f in itertools.chain(*files):
        force_remove(f)


def compare_contents(contents):
    if not contents:
        return ContentsComparison.EMPTY
    comparisons = zip(*contents)
    for comparison in comparisons:
        if comparison and not compare_signatures(comparison):
            return ContentsComparison.DIFFERENT
    return ContentsComparison.EQUAL


if __name__ == "__main__":
    init_buses()
    while True:
        directories = get_directories()
        contents = get_contents(directories)
        if contents_synced(contents):
            compared = compare_contents(contents)
            if compared.are_different():
                clean(contents)
                lock_drawbridge()
            elif compared.are_equal():
                send_to_output(contents[0])
                clean(contents)
