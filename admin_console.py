import os
import hashlib
from anytree import Node
from itertools import count
from distutils.util import strtobool

ADMIN_AUTHUNIT_DIR = os.path.join(os.environ["HOME"],
                                  ".local/share/drawbridge/admin_auth_unit")


def add_user():
    username = input("Input a new username: ")
    password = ""
    while True:
        password = input("Input a new password: ")
        confirm_password = input("Confirm new password: ")
        if password == confirm_password:
            break
        else:
            print("Wrong confirmation, trying again")
    return username, password


def remove_user():
    username = input("Input the username to remove: ")
    return (username,)


def edit_user():
    username = input("Input an existing username: ")
    password = ""
    while True:
        password = input("Input a new password: ")
        confirm_password = input("Confirm new password: ")
        if password == confirm_password:
            break
        else:
            print("Wrong confirmation, trying again")
    return username, password


def pair_exchange():
    key = input("Input key id: ")
    return (key,)


menu_to_action = {
    "Add User": add_user,
    "Remove User": remove_user,
    "Edit User": edit_user,
    "Pair Exchange": pair_exchange,
    "Unpair Exchange": pair_exchange,
    "Maintenance": lambda: (None,)
}


class Menu:
    def __init__(self):
        self.main    = Node("Main Menu")                          # noqa: E221,E501
        auth         = Node("Authentication",  parent=self.main)  # noqa: E221,E501
        add_user     = Node("Add User",        parent=auth)       # noqa: E221,E501,F841
        remove_user  = Node("Remove User",     parent=auth)       # noqa: E221,E501,F841
        edit_user    = Node("Edit User",       parent=auth)       # noqa: E221,E501,F841
        pairing      = Node("Pairing",         parent=self.main)  # noqa: E221,E501
        pair         = Node("Pair Exchange",   parent=pairing)    # noqa: E221,E501,F841
        unpair       = Node("Unpair Exchange", parent=pairing)    # noqa: E221,E501,F841
        maintenance  = Node("Maintenance",     parent=self.main)  # noqa: E221,E501,F841
        self.confirm = Node("Confirm",         parent=self.main)  # noqa: E221,E501
        # TODO policy

    def start(self):
        choices = []
        current = self.main
        while True:
            children = current.children
            for index, child in zip(count(start=1), children):
                print(index, " - ", child.name)
            choice_index = 0
            while True:
                try:
                    if current == self.main:
                        choice_index = int(
                            input("Choose a number(1-%s): " % (len(children)))
                        )
                    else:
                        choice_index = int(
                            input("Choose a number(1-%s, 0 to return): " %
                                  len(children))
                        )
                    if 0 <= choice_index <= len(children):
                        break
                except ValueError:
                    continue
            if choice_index == 0:
                current = self.main
                continue
            choice_index -= 1
            choice = current.children[choice_index]
            if choice.name == self.confirm.name:
                break
            if choice.is_leaf:
                choices.append((choice.name, menu_to_action[choice.name]()))
                current = self.main
            else:
                print(choice)
                current = choice
        return choices


def init_buses():
    for x in [ADMIN_AUTHUNIT_DIR]:
        if not os.path.isdir(x):
            os.makedirs(x)


def send_to_auth_unit(username1, password1, username2, password2, commands):
    strcommands = str(commands)
    basename = hashlib.sha256(strcommands.encode()).hexdigest()
    filename = os.path.join(ADMIN_AUTHUNIT_DIR, basename)
    with open(filename, "w+") as fd:
        fd.write(strcommands)
    with open(filename + ".user1", "w+") as fd:
        fd.write(username1)
        fd.write("\n")
        fd.write(password1)
    with open(filename + ".user2", "w+") as fd:
        fd.write(username2)
        fd.write("\n")
        fd.write(password2)


if __name__ == "__main__":
    init_buses()
    menu = Menu()
    while True:
        username1 = input("Enter 1st username: ")
        password1 = input("Enter 1st password: ")
        commands = menu.start()
        username2 = input("Enter 2nd username: ")
        password2 = input("Enter 2nd password: ")
        print(commands)
        confirm = False
        while True:
            try:
                confirm = bool(strtobool(input("Do you confirm? ")))
            except ValueError:
                continue
            break
        if confirm:
            break
    send_to_auth_unit(username1, password1, username2, password2, commands)
