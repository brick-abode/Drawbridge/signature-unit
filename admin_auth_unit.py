import os
import sqlite3
import bcrypt
import shutil
from pathlib import Path

ADMIN_AUTHUNIT_DIR = os.path.join(os.environ["HOME"],
                                  ".local/share/drawbridge/admin_auth_unit")
ADMIN_UNIT_DIR = os.path.join(os.environ["HOME"],
                              ".local/share/drawbridge/admin_unit")


def init_buses():
    for x in [ADMIN_AUTHUNIT_DIR, ADMIN_UNIT_DIR]:
        if not os.path.isdir(x):
            os.makedirs(x)


def init_db():
    with sqlite3.connect("users.db") as conn:
        c = conn.cursor()
        c.execute(
            '''
            CREATE TABLE IF NOT EXISTS user (
            username TEXT NOT NULL UNIQUE,
            bcrypt_password TEXT NOT NULL
            );
            '''
        )
        conn.commit()


def get_actions():
    return os.listdir(ADMIN_AUTHUNIT_DIR)


def force_remove(filepath):
    if os.path.isfile(filepath) or os.path.isdir(filepath):
        os.remove(filepath)


def check_user(username, password):
    with sqlite3.connect("users.db") as conn:
        c = conn.cursor()
        c.execute(
            '''
            SELECT bcrypt_password
            FROM user
            WHERE username = ?;
            ''',
            (username,)
        )
        user = c.fetchone()
        if user is None:
            return False
        bcrypt_password = user[0]
        return bcrypt.checkpw(password.encode(), bcrypt_password.encode())


def get_user_credentials(entry, idx):
    fullpath = os.path.join(ADMIN_AUTHUNIT_DIR, entry + ".user" + str(idx))
    with open(fullpath, "r") as fd:
        lines = fd.read().splitlines()
        return lines[0], lines[1]


def send_to_admin_unit(entry):
    shutil.move(os.path.join(ADMIN_AUTHUNIT_DIR, entry),
                os.path.join(ADMIN_UNIT_DIR, entry))
    force_remove(os.path.join(ADMIN_AUTHUNIT_DIR, entry + ".user1"))
    force_remove(os.path.join(ADMIN_AUTHUNIT_DIR, entry + ".user2"))


def received_all(entry):
    if Path(entry).suffix != '':
        return False
    fullpath = os.path.join(ADMIN_AUTHUNIT_DIR, entry)
    return os.path.isfile(fullpath) and \
        os.path.isfile(fullpath + ".user1") and \
        os.path.isfile(fullpath + ".user2")


def clean_all(entry):
    for x in ["", ".sig", ".user1", ".user2"]:
        force_remove(os.path.join(ADMIN_AUTHUNIT_DIR, entry + x))


def report_error():
    # TODO send error back to admin console
    pass


if __name__ == "__main__":
    init_buses()
    init_db()
    while True:
        entries = get_actions()
        for entry in entries:
            if not received_all(entry):
                continue
            username1, password1 = get_user_credentials(entry, 1)
            username2, password2 = get_user_credentials(entry, 2)
            if username1 != username2 and \
               check_user(username1, password1) and \
               check_user(username2, password2):
                send_to_admin_unit(entry)
            else:
                clean_all(entry)
                report_error()
