import os
import sqlite3
import struct
from datetime import datetime, timezone
from enum import Enum

CALENDAR_DIR = os.path.join(os.environ["HOME"],
                            ".local/share/drawbridge/calendar")
SECURITY_DIR = os.path.join(os.environ["HOME"],
                            ".local/share/drawbridge/security")


class ActionType(Enum):
    SCHEDULE = 0
    OPEN = 1

    def is_schedule(self):
        return self == self.SCHEDULE

    def is_open(self):
        return self == self.OPEN


class Action():
    def __init__(self, entry):
        fullpath = os.path.join(CALENDAR_DIR, entry)
        with open(fullpath, "r") as fd:
            action_str = [fd.read(1), fd.read(1), fd.read(1)].join("")
            if action_str == "OPN":
                self._type = Action.OPEN
            elif action_str == "SCH":
                self._type = Action.SCHEDULE
                self.begin_sch = datetime.fromtimestamp(
                    struct.unpack("i", fd.read(1))
                )
                self.end_sch = datetime.fromtimestamp(
                    struct.unpack("i", fd.read(1))
                )


def init_buses():
    for x in [CALENDAR_DIR]:
        if not os.path.isdir(x):
            os.makedirs(x)


def init_db():
    with sqlite3.connect("calendar.db") as conn:
        c = conn.cursor()
        c.execute(
            '''
            CREATE TABLE IF NOT EXISTS calendar (
            begin_sch DATETIME NOT NULL UNIQUE,
            end_sch DATETIME TEXT NOT NULL
            );
            '''
        )
        conn.commit()


def force_remove(filepath):
    if os.path.isfile(filepath) or os.path.isdir(filepath):
        os.remove(filepath)


def schedule(begin_sch, end_sch):
    with sqlite3.connect("calendar.db") as conn:
        c = conn.cursor()
        c.execute(
            '''
            INSERT INTO calendar
            (begin_sch, end_sch)
            VALUES
            (?, ?);
            ''',
            (begin_sch, end_sch)
        )
        conn.commit()


def open_console():
    with sqlite3.connect("calendar.db") as conn:
        c = conn.cursor()
        now = datetime.now(timezone.utc)
        c.execute(
            '''
            SELECT begin_sch
            FROM calendar
            WHERE ? >= begin_sch AND ? <= end_sch;
            ''',
            (now, now)
        )
        schedule = c.fetchone()
        if schedule is None:
            # TODO send this to the output console
            print("There was no schedule at this time!")
            return
    # TODO send this to the security engine
    print("Console open")


# Gets the actions to be performed by this unit
def get_actions():
    return os.listdir(CALENDAR_DIR)


if __name__ == "__main__":
    init_buses()
    init_db()
    while True:
        entries = get_actions()
        for entry in entries:
            action = Action(entry)
            if action._type.is_schedule():
                schedule(action.begin_sch, action.end_sch)
            elif action._type.is_open():
                open_console()
            force_remove(entry)
