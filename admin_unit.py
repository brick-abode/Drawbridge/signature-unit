import os
import sqlite3
import bcrypt

ADMIN_UNIT_DIR = os.path.join(os.environ["HOME"],
                              ".local/share/drawbridge/admin_unit")


def add_user(username, password):
    with sqlite3.connect("users.db") as conn:
        c = conn.cursor()
        c.execute(
            '''
            INSERT OR REPLACE INTO user
            (username, bcrypt_password)
            VALUES
            (?, ?)
            ''',
            (username, bcrypt.hashpw(password.encode(), bcrypt.gensalt()))
        )
        conn.commit()


def remove_user(username):
    with sqlite3.connect("users.db") as conn:
        c = conn.cursor()
        c.execute(
            '''
            DELETE FROM user WHERE username = ?
            ''',
            (username,)
        )
        conn.commit()


def edit_user(username, password):
    with sqlite3.connect("users.db") as conn:
        c = conn.cursor()
        c.execute(
            '''
            UPDATE user
            SET bcrypt_password = ?
            WHERE username = ?
            ''',
            (bcrypt.hashpw(password.encode(), bcrypt.gensalt()), username)
        )
        conn.commit()


def pair_exchange(key):
    # TODO add key
    pass


def unpair_exchange(key):
    # TODO remove key
    pass


def maintenance(_dummy):
    # TODO enable maintenance
    pass


command_to_action = {
    "Add User": add_user,
    "Remove User": remove_user,
    "Edit User": edit_user,
    "Pair Exchange": pair_exchange,
    "Unpair Exchange": unpair_exchange,
    "Maintenance": maintenance
}


def init_buses():
    for x in [ADMIN_UNIT_DIR]:
        if not os.path.isdir(x):
            os.makedirs(x)


def get_actions():
    return os.listdir(ADMIN_UNIT_DIR)


def eval_commands(entry):
    with open(os.path.join(ADMIN_UNIT_DIR, entry), "r") as fd:
        return eval(fd.read())


def process_commands(commands):
    for command, parameters in commands:
        action = command_to_action[command]
        action(*parameters)


def finish_command(entry):
    filepath = os.path.join(ADMIN_UNIT_DIR, entry)
    if os.path.isfile(filepath):
        os.remove(filepath)


if __name__ == "__main__":
    init_buses()
    while True:
        entries = get_actions()
        for entry in entries:
            commands = eval_commands(entry)
            process_commands(commands)
            finish_command(entry)
