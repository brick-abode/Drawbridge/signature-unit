CREATE TABLE IF NOT EXISTS user (username TEXT NOT NULL UNIQUE, bcrypt_password TEXT NOT NULL);
INSERT OR REPLACE INTO user (username, bcrypt_password) VALUES ('user1', '$2b$12$BeiyMrCXymd8P1chZ8P3tu1QQh4sq6CIRKINQfPp7V5y0THpp02D.');
INSERT OR REPLACE INTO user (username, bcrypt_password) VALUES ('user2', '$2b$12$BeiyMrCXymd8P1chZ8P3tu1QQh4sq6CIRKINQfPp7V5y0THpp02D.');
