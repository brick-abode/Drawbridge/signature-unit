import os
import sys
import gnupg
from pathlib import Path
import ecdsa
from codecs import decode
from eth_keys import keys


SIGUNIT_INDEX = sys.argv[1]

SIGUNIT_DIR = os.path.join(os.environ["HOME"],
                           ".local/share/drawbridge/sigunit",
                           SIGUNIT_INDEX)

SIGUNIT_OUTPUT_DIR = os.path.join(SIGUNIT_DIR, "output")

BITCOIN_ID = 1
ETHEREUM_ID = 2


def init_buses():
    for f in [SIGUNIT_DIR, SIGUNIT_OUTPUT_DIR]:
        if not os.path.isdir(f):
            os.makedirs(f)


def force_remove(filepath):
    if os.path.isfile(filepath) or os.path.isdir(filepath):
        os.remove(filepath)


def remove_all(filepath):
    for x in [filepath]:
        force_remove(x)


def is_signed(filepath):
    return os.path.isfile(filepath + ".sig")


def is_authenticated(filepath):
    return os.path.isfile(filepath + ".user")


def verify_signature(filepath):
    if (not is_signed(filepath)) or \
       (not is_authenticated(filepath)):
        print("Non signed or non authenticated")
        remove_all(filepath)
        return False

    gpg = gnupg.GPG(gnupghome=os.path.join(os.environ["HOME"],
                                           ".gnupg"))
    with open(filepath + ".sig", "rb") as sig:
        verify = gpg.verify_file(sig, filepath)
        if verify is not None and verify.trust_level >= 4:
            return True

    remove_all(filepath)
    return False


def sign_bitcoin(private_key, hashed_tx):
    # NOTE: .from_string creates the signing key from a BYTE ARRAY
    sk = ecdsa.SigningKey.from_string(
        private_key, curve=ecdsa.SECP256k1)
    signature = sk.sign_digest_deterministic(
        hashed_tx, sigencode=ecdsa.util.sigencode_der_canonize)
    return signature


def sign_ethereum(private_key, hashed_tx):
    sk = keys.PrivateKey(private_key)
    # Signing using SECP256k1
    signature = sk.sign_msg_hash(hashed_tx)
    return signature.to_bytes()


def get_private_key(blockchain_address):
    # TODO implement the function to get the real private key using the
    # blockchain address. Right now using a dummy private_key
    private_key = decode(
        "EDB8513D6628DD7A2C7EFD52B3AAB02492B6C3734C6D2A49254631AF1C7FFB09",
        "hex"
    )
    return private_key


def deserialize_payload(filepath):
    with open(filepath, "rb") as transaction:
        _payload = transaction.read()
        blockchain_id = int.from_bytes(
            _payload[0:4], byteorder="big", signed=False)
        _payload = _payload[4:]
        hashed_tx_len = int.from_bytes(
            _payload[0:4], byteorder="big", signed=False)
        _payload = _payload[4:]
        hashed_tx = _payload[:hashed_tx_len]
        _payload = _payload[hashed_tx_len:]
        address_len = int.from_bytes(
            _payload[0:4], byteorder="big", signed=False)
        _payload = _payload[4:]
        blockchain_address = _payload[:address_len]
    return (blockchain_id, hashed_tx, blockchain_address)


def sign(filepath):
    signature = None
    blockchain_id, hashed_tx, blockchain_address = deserialize_payload(
        filepath)
    private_key = get_private_key(blockchain_address)
    if blockchain_id == BITCOIN_ID:
        signature = sign_bitcoin(private_key, hashed_tx)
    elif blockchain_id == ETHEREUM_ID:
        signature = sign_ethereum(private_key, hashed_tx)
    else:
        pass
    basename = os.path.basename(filepath)
    output_file = os.path.join(SIGUNIT_OUTPUT_DIR, basename)
    with open(output_file, "wb+") as f:
        if signature:
            f.write(signature)
    remove_all(filepath)


def get_actions():
    def valid_file(filename):
        fullpath = os.path.join(SIGUNIT_DIR, filename)
        return os.path.isfile(fullpath) and Path(fullpath).suffix == ''
    return [os.path.join(SIGUNIT_DIR, x) for x in os.listdir(SIGUNIT_DIR)
            if valid_file(x)]


if __name__ == "__main__":
    init_buses()
    while True:
        entries = get_actions()
        for entry in entries:
            print(f"Verifying for {entry}")
            print("File is valid")
            sign(entry)
