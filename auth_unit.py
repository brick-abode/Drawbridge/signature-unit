import os
import sqlite3
import bcrypt
import shutil
from pathlib import Path
from enum import Enum


class Destination(Enum):
    CALENDAR = 0
    SIGNATURE_UNIT = 1

    def is_calendar(self):
        return self == self.CALENDAR

    def is_sigunit(self):
        return self == self.SIGNATURE_UNIT


# TODO this unit should also check for the integrity of the .sig file

AUTHUNIT_DIR = os.path.join(os.environ["HOME"],
                            ".local/share/drawbridge/auth_unit")
FIREWALL_DIR = os.path.join(os.environ["HOME"],
                            ".local/share/drawbridge/firewall")
CALENDAR_DIR = os.path.join(os.environ["HOME"],
                            ".local/share/drawbridge/calendar")


def init_buses():
    for x in [AUTHUNIT_DIR, FIREWALL_DIR, CALENDAR_DIR]:
        if not os.path.isdir(x):
            os.makedirs(x)


def init_db():
    with sqlite3.connect("users.db") as conn:
        c = conn.cursor()
        # TODO Biometrics info should potentially go in the databse,
        # depending on how the SDK of the hardware is
        c.execute(
            '''
            CREATE TABLE IF NOT EXISTS user (
            username TEXT NOT NULL UNIQUE,
            bcrypt_password TEXT NOT NULL
            );
            '''
        )
        conn.commit()


def force_remove(filepath):
    if os.path.isfile(filepath) or os.path.isdir(filepath):
        os.remove(filepath)


def check_user(username, password):
    with sqlite3.connect("users.db") as conn:
        c = conn.cursor()
        c.execute(
            '''
            SELECT bcrypt_password
            FROM user
            WHERE username = ?;
            ''',
            (username,)
        )
        user = c.fetchone()
        if user is None:
            return False
        bcrypt_password = user[0]
        print(bcrypt_password)
        return bcrypt.checkpw(password.encode(), bcrypt_password.encode())


def send_to(destination, entry):
    shutil.move(os.path.join(AUTHUNIT_DIR, entry),
                os.path.join(destination, entry))
    for x in [".sig", ".user"]:
        force_remove(os.path.join(AUTHUNIT_DIR, entry + x))


def send_to_firewall(entry):
    send_to(FIREWALL_DIR, entry)


def send_to_calendar(entry):
    # TODO check BA signature
    send_to(CALENDAR_DIR, entry)


def clean_all(entry):
    for x in ["", ".sig", ".user"]:
        force_remove(os.path.join(AUTHUNIT_DIR, entry + x))


def blink_led():
    print("LED blinking")


# Gets the actions to be performed by this unit
def get_actions():
    return os.listdir(AUTHUNIT_DIR)


def received_all(entry):
    if Path(entry).suffix != '':
        return False
    fullpath = os.path.join(AUTHUNIT_DIR, entry)
    return os.path.isfile(fullpath) and \
        os.path.isfile(fullpath + ".sig") and \
        os.path.isfile(fullpath + ".user")


def check_destination(entry):
    fullpath = os.path.join(AUTHUNIT_DIR, entry)
    with open(fullpath, "r") as fd:
        cld = "".join([fd.read(1), fd.read(1), fd.read(1)])
        if cld == "OPN" or cld == "SCH":
            return Destination.CALENDAR
        return Destination.SIGNATURE_UNIT


def get_user_credentials(entry):
    fullpath = os.path.join(AUTHUNIT_DIR, entry + ".user")
    with open(fullpath, "r") as fd:
        lines = fd.read().splitlines()
        return lines[0], lines[1]


if __name__ == "__main__":
    init_buses()
    init_db()
    while True:
        entries = get_actions()
        for entry in entries:
            if not received_all(entry):
                continue
            username, password = get_user_credentials(entry)
            if check_user(username, password):
                if check_destination(entry).is_sigunit():
                    send_to_firewall(entry)
                else:
                    send_to_calendar(entry)
            else:
                clean_all(entry)
                blink_led()
