import os
import shutil
import hashlib
import sys

AUTHUNIT_DIR = os.path.join(os.environ["HOME"],
                            ".local/share/drawbridge/auth_unit")


def sha256sum(filename):
    h = hashlib.sha256()
    b = bytearray(128 * 1024)
    mv = memoryview(b)
    with open(filename, 'rb', buffering=0) as f:
        for n in iter(lambda: f.readinto(mv), 0):
            h.update(mv[:n])
    return h.hexdigest()


def init_buses():
    if not os.path.isdir(AUTHUNIT_DIR):
        os.makedirs(AUTHUNIT_DIR)


def thumb_scan(username):
    # TODO actually implement thumbprint scanning
    input("Scan your thumbprint")

    def is_thumb_valid():
        # TODO actually implement thumbprint authentication
        return True

    return is_thumb_valid()


def iris_scan(username):
    # TODO actually implement iris scanning
    input("Scan your iris")

    def is_iris_valid():
        # TODO actually implement iris authentication
        return True

    return is_iris_valid()


def request_signature(filepath, username, password):
    checksum = sha256sum(filepath)
    shutil.copy(filepath, os.path.join(AUTHUNIT_DIR, checksum))
    shutil.copy(filepath + ".sig",
                os.path.join(AUTHUNIT_DIR, checksum + ".sig"))
    with open(os.path.join(AUTHUNIT_DIR,
                           checksum + ".user"), "w") as user_file:
        user_file.write(username)
        user_file.write("\n")
        user_file.write(password)


def process_normal_request():
    file_path = sys.argv[1]
    username = input("Enter your username: ")
    password = input("Enter your password: ")
    if thumb_scan(username) and iris_scan(username):
        return file_path, username, password
    else:
        return None


if __name__ == "__main__":
    init_buses()
    res = process_normal_request()
    if res is not None:
        filepath, username, password = res
        request_signature(filepath, username, password)
    else:
        print("Your authentication went wrong, try again")
