import hashlib
import os
from utils import get_local_directory, binary_to_str
from utils import str_to_binary, flip_byte_order
from signature_prototype import sign
from transaction import get_hashed_info
from create_bitcoin_address import sha256_160

# This script creates a segwit transaction with 1 input and 1 output.
# The output address is the same from the intput.
# I have to refactor this script to include multiple outputs, inputs!
# Maybe include this script (segwit transactions) on the transaction.py
# This script was based on the Youtube video:
# https://www.youtube.com/watch?v=9E_m0BxJy5Y&list=PLH4m2oS2ratcy0AoSiiiQg-n45RnRC5xz&index=1
# See: SegWit wallet development guide for more details about the serialization
# https://bitcoincore.org/en/segwit_wallet_dev/
# OBS: The create_bitcoin_address.py can create segwit addresses!

# ==============================================================================
#                  Outside information to build a transaction
# ==============================================================================
utxo_hash = '8a07bf699100868c77a4d19c821e2b718cb83962ace1cc1404ea2d26f0996de8'
utxo_id = 0
utxo_amount = 95000
public_key = \
    "036f5022523fc0b92de2c05ffc347144d52a8e34b69bf61bbd3b900c58a728f9f5"
output_address = "2NB1E3915SdHdQdncVXAYY5icMAKjCch5QP"
output_amount = 90000
# ==============================================================================

public_key = str_to_binary(public_key)
utxo_hash = flip_byte_order(utxo_hash)
_, hashed_out_address = get_hashed_info(output_address)
hashed_pub_key = sha256_160(public_key)

tx_out_count = (1).to_bytes(1, byteorder="little", signed=False)
pk_script_one = str_to_binary(f'a914{hashed_out_address}87')
pk_script_bytes_one = (len(pk_script_one)).to_bytes(
    1, byteorder="little", signed=False)
value_one = int(output_amount).to_bytes(8, byteorder="little", signed=True)

tx_in_count = (1).to_bytes(1, byteorder="little", signed=False)
index = (utxo_id).to_bytes(4, byteorder="little", signed=False)
sequence = bytes.fromhex('ffffffff')
lock_time = (0).to_bytes(4, byteorder="little", signed=False)
amount = (int(utxo_amount).to_bytes(8, byteorder="little", signed=True))

tx_out = (
    value_one
    + pk_script_bytes_one
    + pk_script_one
)

version = (1).to_bytes(4, byteorder="little", signed=False)
previous_output = str_to_binary(utxo_hash) + index

raw_tx = (
    version
    + tx_in_count
    + previous_output
    + bytes.fromhex('00')  # sighash
    + sequence
    + tx_out_count
    + tx_out
    + lock_time
)

print("RAW: " + raw_tx.hex())

hash_type = bytes.fromhex('01000000')


def dSHA256(raw):
    hash_1 = hashlib.sha256(raw).digest()
    hash_2 = hashlib.sha256(hash_1).digest()
    return hash_2


hashPrevouts = dSHA256(previous_output)
hashSequence = dSHA256(sequence)
hashOutputs = dSHA256(tx_out)


redeemScript = bytes.fromhex(f'0014{binary_to_str(hashed_pub_key)}')
# This is the classic P2PKH scriptPubKey
scriptcode = bytes.fromhex(f'76a914{binary_to_str(hashed_pub_key)}88ac')
hash_pre = (
    version
    + hashPrevouts
    + hashSequence
    + previous_output
    + (len(scriptcode).to_bytes(1, byteorder="little", signed=False))
    + scriptcode
    + amount
    + sequence
    + hashOutputs
    + lock_time
    + hash_type
)

sig_hash = dSHA256(hash_pre)

hashed_tx_path = os.path.join(get_local_directory(), "segwit_hashed_tx")
with open(hashed_tx_path, "wb+") as f:
    f.write(sig_hash)

# Drawbridge signing function
sign(hashed_tx_path)
# Reading the signature
signature_path = hashed_tx_path + ".sig"
with open(signature_path, "rb") as f:
    signature = f.read()

witness = (
    (len(signature) + 1).to_bytes(1, byteorder="little", signed=False)
    + signature
    + bytes.fromhex("01")
    + (len(public_key)).to_bytes(1, byteorder="little", signed=False)
    + public_key
)

ser_tx = (
    version
    + bytes.fromhex('00')  # marker
    + bytes.fromhex('01')  # flag
    + tx_in_count
    + previous_output
    + (len(redeemScript) + 1).to_bytes(1, byteorder="little", signed=False)
    + (len(redeemScript)).to_bytes(1, byteorder="little", signed=False)
    + redeemScript
    + sequence
    + tx_out_count
    + tx_out
    + (2).to_bytes(1, byteorder="little", signed=False)
    + witness
    + lock_time
)

print("hashOutputs: " + hashOutputs.hex())
print("hashSequence: " + hashSequence.hex())
print("hashPrevouts: " + hashPrevouts.hex())
print("HASHED_PRE: " + hash_pre.hex())
print("Final Tx Serialized:\n" + ser_tx.hex())
