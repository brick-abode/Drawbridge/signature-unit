import struct
import base58
import hashlib
import yaml
from utils import get_local_directory, binary_to_str
from utils import str_to_binary, flip_byte_order
import os
import sys
from signature_prototype import sign


VERBOSE = False


def debug(display_string):
    if VERBOSE:
        print(display_string)


def get_hashed_info(bitcoin_address):
    hashed_key = base58.b58decode_check(bitcoin_address)[1:]
    hashed_key_len = binary_to_str(struct.pack("<B", len(hashed_key)))
    hashed_key = binary_to_str(hashed_key)
    return (hashed_key_len, hashed_key)


def read_transaction_config(config_path):
    if not config_path:
        config_path = os.path.join(
            get_local_directory(), "transaction_config.yml")
    if not os.path.exists(config_path):
        raise Exception(f"The file: {config_path} doesn't exists!")
    with open(config_path, "r") as transaction_config:
        print(f"Reading transaction information from: {config_path}")
        return yaml.load(transaction_config, Loader=yaml.FullLoader)


def get_raw_inputs_binary(tx, sign_input_index=0):
    all_inputs = b''
    for index, tx_input in enumerate(tx.tx_inputs):
        if index == sign_input_index:
            all_inputs = (
                all_inputs
                + tx_input["utxo_hash"]
                + tx_input["utxo_index"]
                + tx_input["script_pubkey_len"]
                + tx_input["script_pubkey"]
                + tx_input["sequence"]
            )
        else:
            all_inputs = (
                all_inputs
                + tx_input["utxo_hash"]
                + tx_input["utxo_index"]
                + b'\x00'
                + tx_input["sequence"]
            )
    debug(f"All inputs raw:{binary_to_str(all_inputs)}")
    return all_inputs


def get_outputs_binary(tx):
    all_outputs = b''
    for index, tx_output in enumerate(tx.tx_outputs):
        all_outputs = (
            all_outputs
            + tx_output["amount"]
            + tx_output["script_pubkey_len"]
            + tx_output["script_pubkey"]
        )
    debug(f"All outputs:{binary_to_str(all_outputs)}")
    return all_outputs


def get_raw_tx_hashed(tx, sign_input_index=0):
    raw_tx = (
        tx.version
        + tx.tx_input_count
        + get_raw_inputs_binary(tx, sign_input_index)
        + tx.tx_output_count
        + get_outputs_binary(tx)
        + tx.lock_time
        + struct.pack("<L", 1)
    )
    debug(f"Raw tx:\n{binary_to_str(raw_tx)}")
    return hashlib.sha256(hashlib.sha256(raw_tx).digest()).digest()


def build_signature_script(signature, public_key):
    sigscript = (
        signature
        + b'\x01'
        + struct.pack("<B", len(public_key))
        + public_key
    )
    return sigscript


def get_final_input_binary(tx):
    all_inputs = b''
    for index, tx_input in enumerate(tx.tx_inputs):
        all_inputs = (
            all_inputs
            + tx_input["utxo_hash"]
            + tx_input["utxo_index"]
            + struct.pack("<B", len(tx.sigScripts[index]) + 1)
            + struct.pack("<B", tx.signature_len[index] + 1)
            + tx.sigScripts[index]
            + tx_input["sequence"]
        )
    debug(f"All inputs final:{binary_to_str(all_inputs)}")
    return all_inputs


def build_final_tx(tx):
    real_tx = (
        tx.version
        + tx.tx_input_count
        + get_final_input_binary(tx)
        + tx.tx_output_count
        + get_outputs_binary(tx)
        + tx.lock_time
    )
    return real_tx


class Transaction:
    def __init__(self, config_path=None):
        tx_config = read_transaction_config(config_path)
        self.build_header(len(tx_config["inputs"]), len(tx_config["outputs"]))
        self.public_keys = []
        self.tx_inputs = []
        for tx_input in tx_config["inputs"]:
            self.public_keys.append(str_to_binary(
                tx_input["public_key"].strip()))
            self.tx_inputs.append(self.build_input(
                tx_input["utxo_hash"],
                tx_input["utxo_index"],
                tx_input["address"]
            ))

        self.tx_outputs = []
        for tx_output in tx_config["outputs"]:
            self.tx_outputs.append(self.build_output(
                tx_output["address"],
                int(tx_output["amount"])
            ))
        # Variable that will store the signature scripts
        self.sigScripts = []
        # Variable that will store the signature lengths
        self.signature_len = []

    def build_header(self, num_input=1, num_output=2):
        self.version = struct.pack("<L", 1)
        self.tx_input_count = struct.pack("<B", num_input)
        self.tx_output_count = struct.pack("<B", num_output)
        self.lock_time = struct.pack("<L", 0)

    def build_output(self, bitcoin_address, amount):
        hashed_key_len, hashed_key = get_hashed_info(bitcoin_address)
        tx_output = {}
        tx_output["amount"] = struct.pack("<Q", amount)
        # check if is P2SH
        if bitcoin_address[0] in ["2", "3"]:
            tx_output["script_pubkey"] = str_to_binary(
                f"a9{hashed_key_len}{hashed_key}87")
        else:
            tx_output["script_pubkey"] = str_to_binary(
                f"76a9{hashed_key_len}{hashed_key}88ac")
        tx_output["script_pubkey_len"] = struct.pack(
            "<B", len(tx_output["script_pubkey"]))
        return tx_output

    def build_input(self, utxo_hash, utxo_index, bitcoin_address):
        hashed_key_len, hashed_key = get_hashed_info(bitcoin_address)
        tx_input = {}
        tx_input["utxo_hash"] = str_to_binary(flip_byte_order(utxo_hash))
        tx_input["utxo_index"] = struct.pack("<L", utxo_index)
        tx_input["script_pubkey"] = str_to_binary(
            f"76a9{hashed_key_len}{hashed_key}88ac")
        tx_input["script_pubkey_len"] = struct.pack(
            "<B", len(tx_input["script_pubkey"]))
        tx_input["sequence"] = str_to_binary("ffffffff")
        return tx_input


if __name__ == "__main__":
    transaction_debug = os.getenv('TRANSACTION_DEBUG', "0").lower()
    if transaction_debug not in ["0", "false"]:
        VERBOSE = True
    possible_choices = "raw\nsign\nfinal"
    if len(sys.argv) < 2:
        print("One of the following arguments are necessary:")
        print(f"{possible_choices}")
        exit(1)
    tx = Transaction()
    print("Transaction template created!")
    hashed_tx_base_path = os.path.join(get_local_directory(), "hashed_tx")
    if sys.argv[1].lower() == "raw":
        for i in range(len(tx.tx_inputs)):
            hashed_tx_path = f"{hashed_tx_base_path}_{i}"
            hashed_tx = get_raw_tx_hashed(tx, sign_input_index=i)
            print(f"Hashed tx {i+1}:\n{binary_to_str(hashed_tx)}")
            debug(f"Hashed tx {i+1} len (Bytes):\n{len(hashed_tx)}")
            print(f"Saving hashed transaction {i+1} on: {hashed_tx_path}\n")
            with open(hashed_tx_path, "wb+") as f:
                f.write(hashed_tx)

    elif sys.argv[1].lower() == "sign":
        if len(sys.argv) > 2:
            hashed_tx_path = sys.argv[2]
            if os.path.exists(hashed_tx_path):
                print(f"Signing hashed transaction on: {hashed_tx_path}")
                sign(hashed_tx_path)
            else:
                print(f"The file {hashed_tx_path} doesn't exists")
        else:
            print("Missing the path to the file to be signed!")

    elif sys.argv[1].lower() == "final":
        for i in range(len(tx.tx_inputs)):
            signature_path = f"{hashed_tx_base_path}_{i}.sig"
            print(f"Reading signature from: {signature_path}")
            with open(signature_path, "rb") as f:
                signature = f.read()
            debug(f"Signature:\n{binary_to_str(signature)}")
            debug(f"Signature len:\n{len(signature)}")
            signature_script = build_signature_script(
                signature, tx.public_keys[i])
            debug(f"Signature script:\n{binary_to_str(signature_script)}")
            debug(f"Signature script len:\n{len(signature_script)}")
            tx.sigScripts.append(signature_script)
            tx.signature_len.append(len(signature))
        final_tx = build_final_tx(tx)
        print(f"Final transaction len (Bytes):\n{len(final_tx)}")
        print(f"Final Transaction:\n{binary_to_str(final_tx)}")
    else:
        print(f"Invalid argument: {sys.argv[1]}")
        print("Possible choices are:")
        print(f"{possible_choices}")
