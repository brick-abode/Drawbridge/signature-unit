from codecs import decode
import os


def binary_to_str(byte_array):
    return byte_array.hex()


def str_to_binary(var_string):
    return decode(var_string, "hex")


def flip_byte_order(var_string):
    flipped = "".join(reversed([var_string[i:i + 2]
                                for i in range(0, len(var_string), 2)]))
    return flipped


def get_local_directory():
    return os.path.dirname(os.path.realpath(__file__))
