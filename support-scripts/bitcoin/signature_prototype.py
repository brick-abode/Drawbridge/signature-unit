import ecdsa
import os
from codecs import decode
from utils import get_local_directory


def get_private_key():
    # TODO implement the function to get the real private key
    # Right now using a dummy private_key
    private_key = decode(
        "EDB8513D6628DD7A2C7EFD52B3AAB02492B6C3734C6D2A49254631AF1C7FFB09",
        "hex"
    )
    return private_key


def sign(filepath):
    signature = None
    with open(filepath, "rb") as transaction:
        hashed_tx = transaction.read()
        # NOTE: .from_string creates the signing key from a BYTE ARRAY
        sk = ecdsa.SigningKey.from_string(
            get_private_key(), curve=ecdsa.SECP256k1)
        signature = sk.sign_digest_deterministic(
            hashed_tx, sigencode=ecdsa.util.sigencode_der_canonize)

    print(f"Signature:\n{signature.hex()}")
    print(f"Signature len:\n{len(signature)}")
    basename = os.path.basename(filepath)
    output_file = os.path.join(get_local_directory(), basename + ".sig")
    with open(output_file, "wb+") as f:
        if signature:
            f.write(signature)
    # remove_all(filepath)
