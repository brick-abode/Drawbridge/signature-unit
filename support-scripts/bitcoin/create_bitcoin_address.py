import os
import ecdsa
import hashlib
import base58
import argparse

from utils import str_to_binary, binary_to_str, get_local_directory


def sha256_160(data):
    ripemd160 = hashlib.new('ripemd160')
    sha_256_data = hashlib.sha256(data).digest()
    ripemd160.update(sha_256_data)
    hash160_data = ripemd160.digest()
    return hash160_data


def create_segwit_bitcoin_address(private_key,
                                  is_testnet=True):
    """Creates a segwit bitcoin address (Mainnet or Testnet) from a private key.

    The function uses the [ecdsa python
    library](https://github.com/warner/python-ecdsa#usage) to generate the
    compressed public key from the private key. Then it follows the necessary
    steps described into the [Bitcoin Segwit wallet
    documentation](https://bitcoincore.org/en/segwit_wallet_dev) to create the
    final segwit bitcoin address.

    Parameters:
        private_key (bytearray):
            The Private key used to create the
        bitcoin address is_testnet (bool):
            If true creates an address for
            bitcoin Testnet, otherwise, creates an address for bitcoin Mainnet.
        use_compressed_pubkey (bool):
            If true creates the address using the compressed form of
            the public key.

    Returns:
        final_bitcoin_address (str):
            The bitcoin address
    """
    signing_key = ecdsa.SigningKey.from_string(
        private_key, curve=ecdsa.SECP256k1)
    verifying_key = signing_key.verifying_key
    # The first 32 bytes are the x coordinate
    x_coord = verifying_key.to_string()[:32]
    # The last 32 bytes are the y coordinate
    y_coord = verifying_key.to_string()[32:]
    # We need to turn the y_coord into a number.
    if int.from_bytes(y_coord, byteorder="big", signed=True) % 2 == 0:
        public_key = b'\x02' + x_coord
    else:
        public_key = b'\x03' + x_coord
    print("This is your compressed public key:")
    print(binary_to_str(public_key))

    hash160_pubkey = sha256_160(public_key)

    P2WPKH_V0 = b'\x00\x14' + hash160_pubkey
    hash160_P2WPKH_V0 = sha256_160(P2WPKH_V0)
    network_byte = str_to_binary("C4" if is_testnet else "05")
    pre_address = network_byte + hash160_P2WPKH_V0
    checksum = hashlib.sha256(hashlib.sha256(
        pre_address).digest()).digest()[:4]
    binary_address = pre_address + checksum
    final_bitcoin_address = base58.b58encode(binary_address)
    return final_bitcoin_address, public_key


def create_legacy_bitcoin_address(private_key,
                                  is_testnet=True,
                                  use_compressed_pubkey=True):
    """Creates a bitcoin address (Mainnet or Testnet) from a private key.

    The function uses the [ecdsa python
    library](https://github.com/warner/python-ecdsa#usage) to generate the
    compressed/uncompressed public key from the private key. Then it follows the necessary
    steps described into the [Bitcoin address
    documentation](https://en.bitcoin.it/wiki/Technical_background_of_version_1_Bitcoin_addresses)
    to create the final legacy bitcoin address.

    Parameters:
        private_key (bytearray):
            The Private key used to create the bitcoin address
        is_testnet (bool):
            If true creates an address for bitcoin Testnet, otherwise,
            creates an address for bitcoin Mainnet.
        use_compressed_pubkey (bool):
            If true creates the address using the compressed form of the
            public key.

    Returns:
        final_bitcoin_address (str):
            The bitcoin address
    """

    signing_key = ecdsa.SigningKey.from_string(
        private_key, curve=ecdsa.SECP256k1)
    verifying_key = signing_key.verifying_key
    if use_compressed_pubkey:
        # The first 32 bytes are the x coordinate
        x_coord = verifying_key.to_string()[:32]
        # The last 32 bytes are the y coordinate
        y_coord = verifying_key.to_string()[32:]
        # We need to turn the y_coord into a number.
        if int.from_bytes(y_coord, byteorder="big", signed=True) % 2 == 0:
            public_key = b'\x02' + x_coord
        else:
            public_key = b'\x03' + x_coord
        print("This is your compressed public key:")
        print(binary_to_str(public_key))
    else:
        public_key = b'\x04' + verifying_key.to_string()
        print("This is your uncompressed public key:")
        print(binary_to_str(public_key))

    hash160_pubkey = sha256_160(public_key)
    network_byte = str_to_binary("6F" if is_testnet else "00")
    pre_address = network_byte + hash160_pubkey
    checksum = hashlib.sha256(hashlib.sha256(
        pre_address).digest()).digest()[:4]
    binary_address = pre_address + checksum
    final_bitcoin_address = base58.b58encode(binary_address)
    return final_bitcoin_address, public_key


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-t",
        "--type",
        required=True,
        choices=["legacy", "segwit"],
        help="Type of Bitcoin address (Default: Legacy)"
    )
    parser.add_argument(
        "-n",
        "--network",
        choices=["testnet", "mainnet"],
        help="Bitcoin network (Default: Testnet)"
    )

    parser.add_argument(
        "-p",
        "--private_key",
        help="Path to a text file with the private key \
            (Default: ./private.key.txt)"
    )

    parser.add_argument(
        "-c",
        "--compressed_key",
        choices=["true", "false"],
        help=" If it's to use the compressed public key or not\
            Option only valid for legacy address (Default: true)"
    )
    return parser.parse_args()


if __name__ == "__main__":

    args = get_args()
    is_testnet = True
    if args.network and args.network == "mainnet":
        is_testnet = False

    if args.private_key and os.path.exists(args.private_key):
        private_key_path = args.private_key
    else:
        if args.private_key:
            print(f"The file {args.private_key} doesn't exists!")
        root_directory = get_local_directory()
        private_key_path = os.path.join(root_directory, "private_key.txt")
        print(f"Trying to read the private key from {private_key_path}")

    use_compressed_pubkey = True
    if args.compressed_key and args.compressed_key == "false":
        use_compressed_pubkey = False

    random_key_generated = False
    if os.path.exists(private_key_path):
        with open(private_key_path) as input_file:
            private_key = str_to_binary(input_file.readline().strip())
    else:
        print(f"Couldn't find the file {private_key_path}")
        print("Generating a new private key!")
        private_key = os.urandom(32)
        random_key_generated = True

    print(f"This is your private key:\n{binary_to_str(private_key)}")
    if args.type == "legacy":
        final_bitcoin_address, public_key = create_legacy_bitcoin_address(
            private_key,
            is_testnet,
            use_compressed_pubkey
        )
    else:
        final_bitcoin_address, public_key = create_segwit_bitcoin_address(
            private_key,
            is_testnet,
        )
    network_type = "testnet" if is_testnet else "mainnet"
    print(f"This is your {network_type} bitcoin address:")
    print(f"{final_bitcoin_address.decode()}")
    output_file_path = os.path.join(get_local_directory(),
                                    "./public_address.txt")
    if random_key_generated:
        with open(output_file_path, "a+") as output_file:
            output_string = (
                f"Private key: {binary_to_str(private_key)}\n" +
                f"Uncompressed public key: {binary_to_str(public_key)}\n" +
                f"Bitcoin address: {final_bitcoin_address.decode()}\n" +
                "---------------------------------------------\n"
            )
            output_file.write(output_string)
