import os
import sys
import shutil
from pathlib import Path

SIGUNIT_COUNT = int(sys.argv[1])

FIREWALL_DIR = os.path.join(os.environ["HOME"],
                            ".local/share/drawbridge/firewall")

SIGUNIT_DIR = os.path.join(os.environ["HOME"],
                           ".local/share/drawbridge/sigunit")


def init_buses():
    if not os.path.isdir(FIREWALL_DIR):
        os.makedirs(FIREWALL_DIR)
    for sigunit in range(SIGUNIT_COUNT):
        if not os.path.isdir(os.path.join(SIGUNIT_DIR, str(sigunit))):
            os.makedirs(os.path.join(SIGUNIT_DIR, str(sigunit)))


def forward_to_sigunits(filepath):
    # I first move the sig and user files because the
    # sigunit will delete requests that don't contain both
    basename = os.path.basename(filepath)
    for full, base in [(filepath, basename)]:
        for sigunit in range(SIGUNIT_COUNT):
            print(f"Moving file {base} to {sigunit}")
            shutil.copy(full, os.path.join(SIGUNIT_DIR, str(sigunit)))
        os.remove(full)


if __name__ == "__main__":
    init_buses()
    while True:
        entries = os.listdir(FIREWALL_DIR)
        for entry in entries:
            if Path(entry).suffix == '':
                fullpath = os.path.join(FIREWALL_DIR, entry)
                if os.path.isfile(fullpath):
                    print(f"Forwarding to sig unit {fullpath}")
                    forward_to_sigunits(fullpath)
                else:
                    print("Some of the files are missing")
